ActiveAdmin.register Project do
  permit_params :name, :overall_risk, project_risk_attributes: [
	:id,
	:search_string,
	:impact,
	:probability,
      	:emotion_anger_percent,
      	:emotion_disgust_percent,
      	:emotion_fear_percent,
      	:emotion_joy_percent,
        :emotion_sadness_percent,
	:risk_type_id

  ]

  index do
    selectable_column
    id_column
    column :name
    column :overall_risk
    actions
  end

  filter :name
  filter :overall_risk
  

  form do |f|
    f.inputs "projects" do
      f.input :name 
      f.input :overall_risk
    end

    f.has_many :project_risk do |pr|
        pr.input :risk_type
	pr.input :search_string
        pr.input :impact
        pr.input :probability
       	pr.input :emotion_anger_percent
      	pr.input :emotion_disgust_percent
      	pr.input :emotion_fear_percent
      	pr.input :emotion_joy_percent
        pr.input :emotion_sadness_percent

    end

    f.actions
  end

end
