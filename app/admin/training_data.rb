ActiveAdmin.register TrainingData do
  permit_params :text, :emotion_fear_percent, :emotion_anger_percent, :emotion_disgust_percent, :emotion_joy_percent, :emotion_sadness_percent

  index do
    selectable_column
    id_column
    column :text
    column :emotion_anger_percent
    column :emotion_disgust_percent
    column :emotion_fear_percent
    column :emotion_joy_percent
    column :emotion_sadness_percent
    actions
  end

  filter :text
  filter :emotion_anger_percent
  filter :emotion_disgust_percent
  filter :emotion_fear_percent
  filter :emotion_joy_percent
  filter :emotion_sadness_percent


  form do |f|
    f.inputs "training data" do
      f.input :text 
      f.input :emotion_anger_percent
      f.input :emotion_disgust_percent
      f.input :emotion_fear_percent
      f.input :emotion_joy_percent
      f.input :emotion_sadness_percent
    end
    f.actions
  end

end
