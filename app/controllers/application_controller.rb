
require 'tlearn'
require 'net/http'
require 'net/https'
require 'uri'
#require 'easytone'
	
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception



  def tlearn
	tlearn = TLearn::Run.new(
			 :number_of_nodes => 86,
                         :output_nodes    => 41..46,
                         :linear          => 47..86,
                         :weight_limit    => 1.00,
                         :connections     => [{1..81   => 0},
                                              {1..40   => :i1..:i77},
                                              {41..46  => 1..40},
                                              {1..40   => 47..86},
                                              {47..86  => [1..40, {:max => 1.0, :min => 1.0}, :fixed, :one_to_one]}]

		)

	training_data = [{[0, 0, 0] => [0, 1]}],
                 [{[1, 1, 1]  => [1, 0]}]

	tlearn.train(training_data, sweeps = 5200)

	@retu = tlearn.fitness([1, 0, 1], sweeps = 5200)
	render json: @retu
  end

  def string_to_ints(string)
    string = string.downcase
    characters = "abcdefghijklmnopqrstuvwxyz1234567890,.;: ?!$&"
    indexes = [0] * 77
    x = 0
    string.split("").each do |c|
      characters.split("").each_with_index do |n,i|
        if n == c
          indexes[x] = i
          x += 1
        end  
      end
    end    

   return indexes
  end

  def tlearn2
    network = TLearn::Run.new(
#		    :number_of_nodes => 697,
#                    :output_nodes    => 693..697,
#                    #:linear          => 7..10,
#                    :weight_limit    => 1.00,
#                    :connections     => [
#                                         {1..52   => :i1..:i52},
#                                         {53..692  => 1..52},
#					 {693..697 => 53..692}
                                         #{7..10  => [1..4, {:min => 1.0, :max => 1.0}, :fixed, :one_to_one]}
		      
#		    :number_of_nodes => 256,
#                    :output_nodes    => 255..256,
#                    :linear          => 7..10,
#                    :weight_limit    => 1.00,
#                    :connections     => [{1..256   => 0},
#                                         {1..4   => :i1..:i3},
#                                         {1..4  => 7..10},
#                                         {255..256   => 1..4},
#                                         {7..10  => [1..4, {:min => 1.0, :max => 1.0}, :fixed, :one_to_one]}
#					]
			 :number_of_nodes => 86,
                         :output_nodes    => 42..46,
                         :linear          => 47..86,
                         :weight_limit    => 1.00,
                         :connections     => [{1..81   => 0},
                                              {1..41   => :i1..:i77},
                                              {42..46  => 1..41},
                                              {1..41   => 47..86},
                                              {47..86  => [1..40, {:max => 1.0, :min => 1.0}, :fixed, :one_to_one]}]



#	:number_of_nodes 	=> 697,
#	:output_nodes 		=> 693..697,
#	#:linear 		=> 1..692,
#	:weight_limit		=> 1.00,
#	:connections		=> [
#                {1..52		=>	:i1..:i52},
#		{1..697		=> 	0},
#		{53..564	=> 	1..52},
#		{693..697	=>	565..692},
#		{565..692	=>	53..564},
#	]
    )

#    training_data = [	
#	[{self.string_to_ints("I'm so angry").to_a => [1,0,0,0,0]}],
#	[{self.string_to_ints("I'm so sad").to_a => [0,0,0,1,0]}],
#	[{self.string_to_ints("I'm so disgust").to_a => [0,0,1,0,0]}],
#	[{self.string_to_ints("I'm so joyful").to_a => [0,1,0,0,0]}],
#	[{self.string_to_ints("I'm so happy").to_a => [0,0,0,0,1]}],
#    ]
    training_data = []
    TrainingData.all.each do |data|
	training_data.push([{
		self.string_to_ints(data.text).to_a => [
		data.emotion_anger_percent / 100,
		data.emotion_disgust_percent / 100,
		data.emotion_fear_percent / 100,
		data.emotion_joy_percent / 100,
		data.emotion_sadness_percent / 100
	]}])
    end

    #render json: training_data
    #return
    network.train(training_data, sweeps = 300, "/tmp/session")
    @output = network.fitness(self.string_to_ints("Probability of Bankruptcy SHOULD NOT be confused with actual chance of a company
"), sweeps = 100, "/tmp/session" )

    render json: @output
  end
  
  def tone_analyze
    	
    x =  %x(curl -u "263e2761-9bb6-45c4-a978-71f8ed1a8140":"NoHaPWWebciC" -H "Content-Type: application/json" -d "{\\"text\\": \\"{request}\\"}" "https://gateway.watsonplatform.net/tone-analyzer/api/v3/tone?version=2016-05-19")
    render json:  x	
	
	
	
  end

end
