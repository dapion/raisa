class Project < ApplicationRecord

  has_many :project_risk
  accepts_nested_attributes_for :project_risk, allow_destroy: true

end
