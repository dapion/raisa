class ProjectRisk < ApplicationRecord
  belongs_to :project
  belongs_to :risk_type
end
