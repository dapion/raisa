Rails.application.routes.draw do
  get '/tlearn', to: "application#tlearn"
  get '/tlearn2', to: "application#tlearn2"
  get '/tone_analyze', to: "application#tone_analyze"

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
