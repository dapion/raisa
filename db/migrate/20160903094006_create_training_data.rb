class CreateTrainingData < ActiveRecord::Migration[5.0]
  def change
    create_table :training_data do |t|
	t.text :text
	t.integer :emotion_fear_percent
	t.integer :emotion_anger_percent
	t.integer :emotion_disgust_percent
	t.integer :emotion_joy_percent
	t.integer :emotion_sadness_percent
    end
  end
end
