class CreateProjectRisks < ActiveRecord::Migration[5.0]
  def change
    create_table :project_risks do |t|
      t.integer :risk_type_id
      t.string :search_string
      t.integer :impact
      t.integer :probability
      t.integer :project_id
      t.integer :emotion_anger_percent
      t.integer :emotion_disgust_percent
      t.integer :emotion_fear_percent
      t.integer :emotion_joy_percent
      t.integer :emotion_sadness_percent

      t.timestamps
    end
  end
end
